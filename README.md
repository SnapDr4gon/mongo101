# Mongo101

Consultas basicas en mongodb sobre el archivo grades.json

## Índice

- [Requisitos](#requisitos)
- [Instalación](#instalación)
- [USO](#uso)
- [LICENCIA](#licencia)

## Requisitos

- Cliente de mongodb para realizar las consultas

## Instalación

```bash
git clone git@gitlab.com:SnapDr4gon/mongo101.git
cd mongo101
```

## Uso

Primero usaremos un contenedor de docker con mongo, para eso ejecutamos el siguiente comando:

```bash
docker run --name mongodb -d -p 27017:27017 mongodb/mongodb-community-server
```

Ahora importaremos los datos hacia el contenedor:

```bash
mongoimport -d students -c grades --file /path/to/grades.json
```

Ahora para entrar al cliente de mongo ejecute lo siguiente:

```bash
docker exec -ti mongodb mongosh
```

## Licencia

- Jared Flores Martinez
- 353391
- Universidad Autonoma de Chihuahua
- Web Platforms
